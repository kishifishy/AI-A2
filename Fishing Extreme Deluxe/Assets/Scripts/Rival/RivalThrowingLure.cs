﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class RivalThrowingLure : StateBehaviour
{
    [SerializeField]
    private float duration;

    public bool isFishing;
    [SerializeField]
    private bool coroutineRunning = false;

    [SerializeField]
    private GameObject lurePrefab;

    GameObjectVar lureVar;
    GameObjectVar targetedFishVar;

    void Awake()
    {
        lureVar = blackboard.GetGameObjectVar("lure");
        targetedFishVar = blackboard.GetGameObjectVar("targetedFish");
    }

    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *RivalThrowingLure*");

        ThrowLure();
    }
    
    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *RivalThrowingLure*");

        isFishing = false;
        coroutineRunning = false;
        StopAllCoroutines();
    }
    
    void Update()
    {
        // if at any time, the targeted fish disappears, go back to idle
        if (targetedFishVar.Value == null)
        {
            StartCoroutine(LureAnimation(transform.position, false));
            if (!coroutineRunning)
            {
                SendEvent("foundNoFish");
            }
        }
    }

    void ThrowLure()
    {
        if (!coroutineRunning)
        {
            RaycastHit hit;
            Vector3 direction = targetedFishVar.Value.transform.position - transform.position;
            Ray ray = new Ray(transform.position, direction);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null)
                {
                    if (hit.collider.gameObject.name == "Water")
                    {
                        // FISHING
                        if (!isFishing)
                        {
                            StartCoroutine(LureAnimation(new Vector3(hit.point.x, -0.9237781f, hit.point.z), true));
                            SendEvent("threwLure");
                        }
                    }
                }
            }
        }
    }

    IEnumerator LureAnimation(Vector3 destination, bool spawningLure)
    {
        coroutineRunning = true;

        isFishing = spawningLure;

        // only spawn lure if we're actually supposed to be spawning it
        if (spawningLure)
        {
            GameObject lure = Instantiate(lurePrefab, transform.position, Quaternion.identity);
            lure.transform.parent = this.transform;
            lureVar.Value = lure;
        }
        
        // move the lure towards the destination (water or back to player)
        iTween.MoveTo(lureVar.Value, iTween.Hash(
            "position", destination,
            "time", duration));

        yield return new WaitForSeconds(duration);

        // if we were reeling in, destroy the lure
        if (!spawningLure)
            Destroy(lureVar.Value);

        coroutineRunning = false;
    }
}


