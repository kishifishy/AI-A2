﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class RivalSneaking : StateBehaviour
{
    GameObjectVar playerVar;
    GameObjectVar playerFishBoxVar;

    NavMeshAgent navMeshAgent;
    NavMeshPath navMeshPath;

    void Awake()
    {
        playerVar = blackboard.GetGameObjectVar("player");
        playerFishBoxVar = blackboard.GetGameObjectVar("playerFishBox");

        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshPath = new NavMeshPath();
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *RivalSneaking*");
        
        navMeshAgent.speed *= 0.5f;
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *RivalSneaking*");
    }

    // Update is called once per frame
    void Update()
    {
        navMeshAgent.CalculatePath(playerFishBoxVar.Value.transform.position, navMeshPath);
        navMeshAgent.SetPath(navMeshPath);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Fish Box")
        {
            if (other.gameObject == playerFishBoxVar.Value)
            {
                SendEvent("arrivedAtPlayersBox");
            }
        }
        else if (other.gameObject == playerVar.Value)
        {
            SendEvent("caughtByPlayer");
        }
    }
}


