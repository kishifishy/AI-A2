﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowingLure : MonoBehaviour
{
    public bool isFishing;
    public bool atFishBox;
    private bool coroutineRunning = false;

    [SerializeField]
    private float duration;

    [SerializeField]
    private GameObject playerFishBox, rivalFishBox, lurePrefab;
    [HideInInspector]
    public GameObject lure;

    void Start()
    {
        isFishing = false;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!coroutineRunning)
            {
                if (!atFishBox)
                {
                    RaycastHit hit;
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.collider != null)
                        {
                            if (hit.collider.gameObject.name == "Water")
                            {
                                // FISHING
                                if (!isFishing)
                                    StartCoroutine(LureAnimation(new Vector3(hit.point.x, -0.9237781f, hit.point.z), true));
                                // REELING IN
                                else if (isFishing)
                                    StartCoroutine(LureAnimation(transform.position, false));
                            }
                        }
                    }
                }
                // PUTTING FISH IN FISH BOX
                else if (atFishBox)
                {
                    if (transform.GetChild(0).GetChild(0).childCount > 0)
                    {
                        playerFishBox.GetComponent<FishBox>().AddFish(1);
                        Destroy(transform.GetChild(0).GetChild(0).GetChild(0).gameObject);
                    }
                }
            }
        }

        // cancel fishing if player moves
        else if (isFishing &&
            (Input.GetAxis("Vertical") >= 0.01f ||
            Input.GetAxis("Horizontal") >= 0.01f))
        {
            StartCoroutine(LureAnimation(transform.position, false));
        }
    }

    IEnumerator LureAnimation(Vector3 destination, bool spawningLure)
    {
        coroutineRunning = true;

        isFishing = spawningLure;

        // only spawn lure if we're actually supposed to be spawning it
        if (spawningLure)
        {
            lure = Instantiate(lurePrefab, transform.position, Quaternion.identity);
            lure.transform.parent = this.transform;
        }

        // move the lure towards the destination (water or back to player)
        iTween.MoveTo(lure, iTween.Hash(
            "position", destination,
            "time", duration));

        yield return new WaitForSeconds(duration);
        
        // if we were reeling in, destroy the lure
        if (!spawningLure)
            Destroy(lure);

        coroutineRunning = false;
    }

    // POSSIBLY PUT THIS ON AN EXTIRELY DIFFERENT SCRIPT ?? :/

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Fish Box")
        {
            if (other.gameObject == playerFishBox) {
                atFishBox = true;
            }
            
            if (other.gameObject == rivalFishBox)
            {
                // steal?
            }  
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Fish Box")
        {
            if (other.gameObject == playerFishBox)
            {
                atFishBox = false;
            }

            if (other.gameObject == rivalFishBox)
            {
                // steal?
            }
        }
    }
}
