﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class RivalStealing : StateBehaviour
{
    [SerializeField]
    private GameObject fishModelPrefab;
    private GameObject fish;

    GameObjectVar playerFishBoxVar;
    GameObjectVar rivalFishBoxVar;

    void Awake()
    {
        playerFishBoxVar = blackboard.GetGameObjectVar("playerFishBox");
        rivalFishBoxVar = blackboard.GetGameObjectVar("rivalFishBox");
    }

    void OnEnable()
    {
        //Debug.Log("Started *RivalPuttingFishInBox*");

        // kind of like a timer
        StartCoroutine(StealFish());
    }

    void OnDisable()
    {
        //Debug.Log("Stopped *RivalPuttingFishInBox*");

        Destroy(fish);
        StopAllCoroutines();
    }

    void Update()
    {
    }

    IEnumerator StealFish()
    {
        yield return new WaitForSeconds(1.0f);

        fish = Instantiate(fishModelPrefab, playerFishBoxVar.Value.transform.position, Quaternion.identity);
        iTween.MoveTo(fish, iTween.Hash(
            "position", rivalFishBoxVar.Value.transform.position,
            "orienttopath", true,
            "time", 1.0f,
            "easetype", "easeOutBack"));

        if (playerFishBoxVar.Value.GetComponent<FishBox>().fishInsideBox >= 1)
        {
            playerFishBoxVar.Value.GetComponent<FishBox>().AddFish(-1);
            rivalFishBoxVar.Value.GetComponent<FishBox>().AddFish(1);
        }

        yield return new WaitForSeconds(0.5f);

        Destroy(fish);

        SendEvent("finishedStealing");

    }
}


