﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;

public class Wandering : MonoBehaviour
{

    public float wanderRadius;
    public Vector3 destination;
    NavMeshAgent navMeshAgent;
    NavMeshPath navMeshPath;

    public float arrivalDistance;

    void Awake ()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshPath = new NavMeshPath();
    }
    // Use this for initialization
    void Start ()
    {
        SelectRandomDestination();
    }

    // Update is called once per frame
    void Update ()
    {
        if (navMeshAgent.remainingDistance <= arrivalDistance)
        {
            SelectRandomDestination();
        }

        navMeshAgent.CalculatePath(destination, navMeshPath);
        navMeshAgent.SetPath(navMeshPath);
    }


    void SelectRandomDestination ()
    {

        Vector3 randomPosition = Quaternion.Euler(new Vector3(90, 0, 0)) * Random.insideUnitCircle * wanderRadius;
        NavMeshHit hit;
        NavMesh.SamplePosition(randomPosition, out hit, wanderRadius, 8);
        destination = hit.position;

    }

}
