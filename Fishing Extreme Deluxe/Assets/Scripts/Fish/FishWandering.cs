﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class FishWandering : StateBehaviour
{
    [SerializeField]
    private float wanderRadius, arrivalDistance;
    
    private Vector3 destination;

    GameObjectVar lureGameObjectVar;

    NavMeshAgent navMeshAgent;
    NavMeshPath navMeshPath;

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshPath = new NavMeshPath();

        lureGameObjectVar = blackboard.GetGameObjectVar("lure");
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *FishWandering*");

        SelectRandomDestination();
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *FishWandering*");

        CancelInvoke();
    }

    // Update is called once per frame
    void Update()
    {
        if (navMeshAgent.remainingDistance <= arrivalDistance)
        {
            SelectRandomDestination();
        }

        navMeshAgent.CalculatePath(destination, navMeshPath);
        navMeshAgent.SetPath(navMeshPath);
    }

    void SelectRandomDestination()
    {
        Vector3 randomPosition = Quaternion.Euler(new Vector3(90, 0, 0)) * Random.insideUnitCircle * wanderRadius;

        NavMeshHit hit;

        NavMesh.SamplePosition(randomPosition, out hit, wanderRadius, 9);

        destination = hit.position;
    }

    void OnVisionEnter(GameObject other)
    {
        if (other.gameObject.tag == "Lure")
        {
            GameObject lureDetected = other.gameObject;
            Lure lureScript = lureDetected.GetComponent<Lure>();

            // only swim to lure if other fish haven't seen it yet
            if (lureScript.isTargeted == false)
            {
                lureGameObjectVar.Value = lureDetected;
                SendEvent("seeLure");
            }
        }

    }

    void OnVisionExit()
    {

    }
}


