﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class FishBiting : StateBehaviour
{
    [SerializeField]
    private float bitingTime;

    FloatVar shakeMagnitudeVar;

    GameObjectVar fisherVar;
    GameObjectVar lureGameObjectVar;

    void Awake()
    {
        shakeMagnitudeVar = blackboard.GetFloatVar("shakeMagnitude");
        fisherVar = blackboard.GetGameObjectVar("fisher");
        lureGameObjectVar = blackboard.GetGameObjectVar("lure");
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *FishBiting*");

        fisherVar.Value = lureGameObjectVar.Value.GetComponent<Lure>().owner;

        // kind of like a countdown to when time's up
        StartCoroutine(ReleaseBite());
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *FishBiting*");

        StopAllCoroutines();
    }

    // Update is called once per frame
    void Update()
    {
        if (lureGameObjectVar.Value != null)
        {

            iTween.ShakeRotation(this.gameObject, iTween.Hash(
                "y", shakeMagnitudeVar.Value * 3.0f,
                "time", bitingTime));

            iTween.ShakePosition(this.gameObject, iTween.Hash(
                "x", shakeMagnitudeVar.Value * 0.001f,
                "z", shakeMagnitudeVar.Value * 0.001f,
                "time", bitingTime));
        }
        // player caught you at the right time!!!
        else
        {
            lureGameObjectVar.Value = null;
            SendEvent("playerReelsIn");
        }
    }

    IEnumerator ReleaseBite()
    {
        yield return new WaitForSeconds(bitingTime);

        // if player doesn't catch you while you're biting, flee
        SendEvent("loseLure");
    }
}


