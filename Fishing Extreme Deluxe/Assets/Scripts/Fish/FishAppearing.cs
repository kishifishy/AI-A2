﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class FishAppearing : StateBehaviour
{
    [SerializeField]
    private float minScale, maxScale;

    private float targetScale;

    NavMeshAgent navMeshAgent;

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        targetScale = Random.Range(minScale, maxScale);

        // bigger fish are slower, smaller fish are faster
        navMeshAgent.speed -= targetScale * 0.5f;
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *FishAppearing*");
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *FishAppearing*");
    }

    // Update is called once per frame
    void Update()
    {
        iTween.ScaleTo(gameObject, iTween.Hash(
            "scale", new Vector3(targetScale, targetScale, targetScale),
            "time", 0.5f,
            "easetype", "easeOutBack",
            "oncomplete", "FinishAppearing"));
    }

    void FinishAppearing()
    {
        if (transform.localScale.x >= targetScale)
        {
            // lock scale
            transform.localScale = new Vector3(targetScale, targetScale, targetScale);

            // MOVE TO NEXT STATE
            SendEvent("doneAppearing");
        }
    }
}


