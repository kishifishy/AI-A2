﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseRotation : MonoBehaviour
{

    GameObject model;

    void Awake()
    {
        model = transform.GetChild(0).gameObject;
    }

    void LateUpdate()
    {
        RotateToMouse();
    }

    void RotateToMouse()
    {
        Vector3 mousePosition;
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            mousePosition = hit.point;
            mousePosition.y = model.transform.position.y;
            model.transform.LookAt(mousePosition);
        }

    }
}
