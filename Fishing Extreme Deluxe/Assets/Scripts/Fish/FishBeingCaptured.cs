﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class FishBeingCaptured : StateBehaviour
{
    [SerializeField]
    private float duration;

    Transform fishHolder;

    GameObjectVar fisherVar;
    GameObjectVar lureVar;

    private void Awake()
    {
        fisherVar = blackboard.GetGameObjectVar("fisher");
        lureVar = blackboard.GetGameObjectVar("lure");
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *FishBeingCaptured*");

        // remove self from list
        GetComponentInParent<Spawner>().fishInPond.Remove(this.gameObject);

        // get the Fish Holder for reference
        fishHolder = fisherVar.Value.transform.GetChild(0).GetChild(0);

        // animate the fish going to the player
        StartCoroutine(MoveFish());

        // parent model to the lure
        //GameObject model = transform.GetChild(1).gameObject;


        // now delete
        //Destroy(this.gameObject);
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *FishBeingCaptured*");
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator MoveFish()
    {
        iTween.MoveTo(this.gameObject, iTween.Hash(
            "position", fishHolder.position,
            "orienttopath", true,
            "time", 1.0f,
            "easetype", "easeOutBack"));

        yield return new WaitForSeconds(duration);

        GameObject model = transform.GetChild(1).gameObject;
        model.transform.parent = fishHolder.transform;
        transform.position = fishHolder.transform.position;
        transform.rotation = Quaternion.Euler(Vector3.zero);

        Destroy(this.gameObject);
    }
}


