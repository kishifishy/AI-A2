//Maya ASCII 2018 scene
//Name: Fisher.ma
//Last modified: Tue, Nov 07, 2017 05:48:00 PM
//Codeset: 1252
requires maya "2018";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "2514D386-487B-4D85-2540-4D9ABC08773F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 154.93335658342528 60.716069613212348 367.2738398918612 ;
	setAttr ".r" -type "double3" 2.6616472701656968 1460.9999999999532 2.1292720150233701e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "C8E3B12C-4CFB-8CB6-6F7C-EA900BEFB3C1";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 426.8431892835207;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "934D3486-4541-F802-20CA-2E9291FD3E1F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -11.410278593867133 1000.1 -34.282604480745988 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "FB06BFAA-4C59-8A9C-5880-6A97AB7E9567";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 501.70213249030866;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "56E81070-42E6-A2E9-FA7F-0CB9E25EAB69";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -6.2515350660563165 74.97733316949595 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "07F75A2B-4487-04D0-A9CE-4FA6FDB73EA4";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 173.35414513708284;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "38D33449-4048-8B78-B17B-6CAEEBF73C42";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1002.2873351130359 64.252866782264647 3.2722954499174932 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "9C8C9355-4773-5C20-8EC4-B29CB86D6AD5";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1002.2873381668311;
	setAttr ".ow" 51.714649895876285;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" -3.0537952095954779e-06 66.180753556833338 -4.5806928152813953e-06 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "reference_front";
	rename -uid "A24073D9-474A-4D47-35C7-E7B58B69D1F0";
	setAttr ".t" -type "double3" 0.36099303784278369 50 -50 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 100 100 100 ;
createNode mesh -n "reference_frontShape" -p "reference_front";
	rename -uid "9CBC4E08-4C6D-5117-4CD9-5584A9570082";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -0.5 -1.110223e-16 0.5 0.5 -1.110223e-16 0.5
		 -0.5 1.110223e-16 -0.5 0.5 1.110223e-16 -0.5;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "head";
	rename -uid "1CE6BFF2-4327-C94E-F428-86B09318C6F1";
	setAttr ".t" -type "double3" 0 77.328395513976375 0 ;
	setAttr ".s" -type "double3" 22.709308686544048 21.514081930637055 21.514081930637055 ;
createNode mesh -n "headShape" -p "head";
	rename -uid "B750E66A-4AF6-39DC-65DE-F7B52F521A99";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49914021790027618 0.97850260138511658 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 77 ".pt";
	setAttr ".pt[64]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[65]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[66]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[67]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[68]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[69]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[70]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[71]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[72]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[73]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[74]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[75]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[76]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[77]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[78]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[79]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[80]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[81]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[82]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[83]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[84]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[85]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[86]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[87]" -type "float3" 0 -0.061275329 0 ;
	setAttr ".pt[192]" -type "float3" 0 0 -0.010971571 ;
	setAttr ".pt[195]" -type "float3" 0 0 -0.013493895 ;
	setAttr ".pt[196]" -type "float3" 0 0 -0.017664598 ;
	setAttr ".pt[199]" -type "float3" 0 0 -0.017664598 ;
	setAttr ".pt[200]" -type "float3" 0 0 -0.013493895 ;
	setAttr ".pt[203]" -type "float3" 0 0 -0.010971571 ;
	setAttr ".pt[204]" -type "float3" 0 0 -0.00092308619 ;
	setAttr ".pt[207]" -type "float3" 0 0 0.0025405795 ;
	setAttr ".pt[208]" -type "float3" 0 0 0.011694723 ;
	setAttr ".pt[211]" -type "float3" 0 0 0.014138518 ;
	setAttr ".pt[212]" -type "float3" 0 0 0.017664598 ;
	setAttr ".pt[215]" -type "float3" 0 0 0.017664598 ;
	setAttr ".pt[216]" -type "float3" 0 0 0.014138518 ;
	setAttr ".pt[219]" -type "float3" 0 0 0.011694723 ;
	setAttr ".pt[220]" -type "float3" 0 0 0.0025405795 ;
	setAttr ".pt[222]" -type "float3" 8.9406967e-08 0 0 ;
	setAttr ".pt[223]" -type "float3" 0 0 -0.00092308619 ;
	setAttr ".pt[224]" -type "float3" 0 0 -0.035929441 ;
	setAttr ".pt[225]" -type "float3" 0 0 -0.042101435 ;
	setAttr ".pt[226]" -type "float3" 0 0 -0.025221404 ;
	setAttr ".pt[227]" -type "float3" 0 0 -0.0318061 ;
	setAttr ".pt[228]" -type "float3" 0 0 -0.04359924 ;
	setAttr ".pt[229]" -type "float3" 0 0 -0.052411072 ;
	setAttr ".pt[230]" -type "float3" 0 0 -0.043429647 ;
	setAttr ".pt[231]" -type "float3" 0 0 -0.052411072 ;
	setAttr ".pt[232]" -type "float3" 0 0 -0.025442356 ;
	setAttr ".pt[233]" -type "float3" 0 0 -0.0318061 ;
	setAttr ".pt[234]" -type "float3" 0 0 -0.035809755 ;
	setAttr ".pt[235]" -type "float3" 0 0 -0.042101435 ;
	setAttr ".pt[236]" -type "float3" 0 0 0.0064926292 ;
	setAttr ".pt[237]" -type "float3" 0 0 0.0070617823 ;
	setAttr ".pt[238]" -type "float3" 0 0 -0.0062255161 ;
	setAttr ".pt[239]" -type "float3" 0 0 -0.0067934124 ;
	setAttr ".pt[240]" -type "float3" 0 0 0.036075123 ;
	setAttr ".pt[241]" -type "float3" 0 0 0.041917033 ;
	setAttr ".pt[242]" -type "float3" 0 0 0.025328208 ;
	setAttr ".pt[243]" -type "float3" 0 0 0.031416509 ;
	setAttr ".pt[244]" -type "float3" 0 0 0.043836948 ;
	setAttr ".pt[245]" -type "float3" 0 0 0.052411072 ;
	setAttr ".pt[246]" -type "float3" 0 0 0.043675072 ;
	setAttr ".pt[247]" -type "float3" 0 0 0.052411072 ;
	setAttr ".pt[248]" -type "float3" 0 0 0.025504947 ;
	setAttr ".pt[249]" -type "float3" 0 0 0.031416509 ;
	setAttr ".pt[250]" -type "float3" 0 0 0.035921905 ;
	setAttr ".pt[251]" -type "float3" 0 0 0.041917026 ;
	setAttr ".pt[252]" -type "float3" 0 0 -0.006217673 ;
	setAttr ".pt[253]" -type "float3" 0 0 -0.0067934142 ;
	setAttr ".pt[254]" -type "float3" 0 0 0.006500388 ;
	setAttr ".pt[255]" -type "float3" 0 0 0.0070617842 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "head";
	rename -uid "458AAD4F-4ABC-4737-2BF9-94996E1B55AA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.24854187667369843 0.60106939077377319 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 137 ".uvst[0].uvsp[0:136]" -type "float2" 0.0073384643 0.010159086
		 0.86006272 0.010520201 0.49342331 0.010147911 0.14000157 0.010520201 0.25368565 0.010881227
		 0.3673692 0.010520112 0.50003189 0.010159012 0.63269395 0.010520201 0.746378 0.010881227
		 0.0073386133 0.50284129 0.0073386133 0.48081851 0.12929416 0.50284129 0.13096663
		 0.48085093 0.25368661 0.50284129 0.25368649 0.48088318 0.37807801 0.50284129 0.37640542
		 0.48085093 0.50003183 0.50284129 0.50003183 0.48081851 0.62198561 0.50284129 0.62365824
		 0.48085093 0.74637711 0.50284129 0.74637723 0.48088318 0.0073386133 0.4681446 0.85481012
		 0.46818572 0.0073385537 0.40564656 0.14525369 0.46818572 0.14746284 0.40573132 0.25368601
		 0.46822691 0.25368583 0.40581623 0.36211795 0.46818572 0.3599084 0.40573132 0.50003183
		 0.4681446 0.50003183 0.40564656 0.63794565 0.46818572 0.64015508 0.40573132 0.74637771
		 0.46822691 0.74637794 0.40581623 0.0073384643 0.041110151 0.13979885 0.041110151
		 0.25368565 0.041110151 0.36757195 0.041110151 0.50003189 0.041110151 0.63249129 0.041110151
		 0.746378 0.041110151 0.99272513 0.010159086 0.99272513 0.041110151 0.86026537 0.041110151
		 0.99272531 0.40564656 0.99272531 0.4681446 0.86909717 0.48085093 0.85260105 0.40573132
		 0.99272531 0.48081851 0.87076962 0.50284129 0.99272531 0.50284129 0.84106106 0.55199969
		 0.74973863 0.55199969 0.74973863 0.60106939 0.89938551 0.60106939 0.65841609 0.55199969
		 0.60009146 0.60106939 0.63525701 0.55199969 0.048708797 0.60106939 0.15721944 0.55199969
		 0.098894842 0.60106939 0.24854188 0.55199969 0.24854188 0.60106939 0.3398644 0.55199969
		 0.39818892 0.60106939 0.36302355 0.55199969 0.94957167 0.60106939 0.74973863 0.67450744
		 0.9202252 0.67450744 0.57925165 0.67450744 0.012620032 0.67450744 0.078055121 0.67450744
		 0.24854188 0.67450744 0.41902867 0.67450744 0.98566061 0.67450744 0.74973863 0.76113325
		 0.9209649 0.76113325 0.57851219 0.76113325 0.0044564009 0.76113325 0.077315487 0.76113325
		 0.24854188 0.76113325 0.41976824 0.76113325 0.99382406 0.76113325 0.74973863 0.84775937
		 0.90569717 0.84775937 0.59378016 0.84775937 0.028429748 0.84775937 0.092583336 0.84775937
		 0.24854188 0.84775937 0.4045004 0.84775937 0.96985072 0.84775937 0.74973863 0.92119735
		 0.86765987 0.92119735 0.63181746 0.92119735 0.083562024 0.92119735 0.13062064 0.92119735
		 0.24854188 0.92119735 0.36646309 0.92119735 0.91471845 0.92119735 0.74973863 0.97026694
		 0.81452364 0.97026694 0.68495345 0.97026694 0.15692195 0.97026694 0.18375677 0.97026694
		 0.24854188 0.97026694 0.31332693 0.97026694 0.84135848 0.97026694 0.24854188 0.98673826
		 0.28507084 0.54166108 0.29433459 0.54166108 0.24854188 0.54166108 0.21201292 0.54166108
		 0.70394588 0.54166108 0.71320963 0.54166108 0.74973863 0.54166108 0.78626752 0.54166108
		 0.20274922 0.54166108 0.13406025 0.55199969 0.79553127 0.54166108 0.86422032 0.55199969
		 0.34016183 0.97026694 0.74973863 0.98673826 0.65811861 0.97026694 0.41352174 0.92119735
		 0.58475876 0.92119735 0.4686541 0.84775937 0.52962649 0.84775937 0.49262732 0.76113325
		 0.50565314 0.76113325 0.48446378 0.67450744 0.51381683 0.67450744 0.44837502 0.60106939
		 0.54990554 0.60106939;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt";
	setAttr ".pt[13]" -type "float3" 0 0 -0.036507096 ;
	setAttr ".pt[21]" -type "float3" 0 0 -0.089608379 ;
	setAttr ".pt[29]" -type "float3" 0 0 -0.089608379 ;
	setAttr ".pt[37]" -type "float3" 0 0 -0.056420084 ;
	setAttr -s 106 ".vt[0:105]"  0.38144094 -0.92387962 -0.40893823 0 -0.92387962 -0.57832617
		 -0.38144094 -0.92387962 -0.40893823 -0.47817329 -0.92387962 -1.9544697e-08 -0.38144094 -0.92387962 0.40893838
		 0 -0.92387962 0.57832611 0.38144094 -0.92387962 0.40893838 0.47817349 -0.92387962 -1.9544697e-08
		 0.6250543 -0.70710683 -0.65117508 0 -0.70710683 -0.92090094 -0.6250543 -0.70710683 -0.65117508
		 -0.83467418 -0.70710683 0 -0.6250543 -0.70710683 0.60405439 0 -0.70710683 0.87378025
		 0.62505436 -0.70710683 0.60405439 0.83467418 -0.70710683 0 0.71209866 -0.38268352 -0.72357619
		 0 -0.38268352 -1.023291349 -0.71209866 -0.38268352 -0.72357619 -0.98541188 -0.38268352 -2.8756728e-09
		 -0.71209866 -0.38268352 0.72357619 0 -0.38268352 1.023291349 0.71209872 -0.38268352 0.72357631
		 0.98541206 -0.38268352 -2.8756728e-09 0.71518815 0 -0.73469847 0 0 -1.039020538 -0.71518815 0 -0.73469847
		 -1.01951015 0 -1.1629058e-09 -0.71518815 0 0.73469847 0 0 1.039020538 0.71518821 0 0.73469853
		 1.019510269 0 -1.1629058e-09 0.65141642 0.38268352 -0.64691406 0 0.38268352 -0.91487467
		 -0.65141642 0.38268352 -0.64691406 -0.91937697 0.38268352 2.9047384e-10 -0.65141642 0.38268352 0.64691406
		 0 0.38268352 0.91487467 0.65141648 0.38268352 0.64691412 0.91937715 0.38268352 2.9047384e-10
		 0.49254015 0.70710683 -0.47453067 0 0.70710683 -0.67108768 -0.49254015 0.70710683 -0.47453067
		 -0.68909717 0.70710683 0 -0.49254015 0.70710683 0.47453067 0 0.70710683 0.67108768
		 0.49254018 0.70710683 0.4745307 0.68909729 0.70710683 0 0.27059805 0.92387938 -0.27059805
		 0 0.92387938 -0.3826834 -0.27059805 0.92387938 -0.27059805 -0.3826834 0.92387938 0
		 -0.27059805 0.92387938 0.27059805 0 0.92387938 0.38268343 0.27059805 0.92387938 0.27059805
		 0.38268343 0.92387938 0 0 0.99664426 0 0.1912694 -0.9695518 -7.8178788e-09 0.15257637 -0.9695518 0.16357537
		 0 -0.9695518 0.23133044 -0.15257637 -0.9695518 0.16357537 -0.19126934 -0.9695518 -7.8178788e-09
		 -0.15257637 -0.9695518 -0.16357529 0 -0.9695518 -0.23133048 0.15257637 -0.9695518 -0.16357529
		 0 -1.087077856 -0.24679968 0.16533817 -1.086905241 -0.1735162 -0.16533817 -1.086905241 -0.1735162
		 -0.20992348 -1.086732864 -9.3504191e-09 -0.16533816 -1.086905241 0.17351629 0 -1.087077856 0.24679963
		 0.16533814 -1.086905241 0.17351629 0.20992357 -1.086732864 -9.6967305e-09 3.3745074e-08 -3.59878039 -0.85134274
		 0.61210322 -3.59685326 -0.57229888 1.6786705e-08 -3.59884 -2.2368408e-07 -0.61210352 -3.59685326 -0.57229543
		 -0.84178913 -3.5949266 9.2617415e-07 -0.61210328 -3.59685373 0.57229471 3.3745074e-08 -3.59878063 0.8513388
		 0.61210287 -3.59685326 0.57229805 0.84178919 -3.5949266 9.0130425e-07 -1.8517174e-08 -1.15471315 -0.33986086
		 0.27224261 -1.15449309 -0.23785323 0.3487955 -1.15427375 6.0050411e-08 0.27224255 -1.15449309 0.23785338
		 -1.8517174e-08 -1.15471315 0.33986071 -0.27224264 -1.15449309 0.23785323 -0.34879541 -1.15427375 6.1511301e-08
		 -0.2722427 -1.15449309 -0.23785309 -1.6844581e-08 -1.48823786 -0.46597704 -0.37922704 -1.4877851 -0.32192954
		 -0.4978393 -1.48733234 3.0769348e-07 -0.37922704 -1.4877851 0.32192972 -1.6844581e-08 -1.48823786 0.46597639
		 0.37922689 -1.4877851 0.32193047 0.49783948 -1.48733234 3.0191521e-07 0.37922695 -1.4877851 -0.32193032
		 3.0696079e-08 -3.43360806 -0.95315278 -0.68462932 -3.43360806 -0.6417734 -0.94012362 -3.43360806 1.3218018e-06
		 -0.68462902 -3.43360806 0.64177322 3.0696068e-08 -3.43360806 0.95314896 0.68462849 -3.43360806 0.64177704
		 0.9401238 -3.43360806 1.2946314e-06 0.68462896 -3.43360806 -0.64177728;
	setAttr -s 216 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 0 0 8 9 0
		 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 16 17 0 17 18 0 18 19 0 19 20 0
		 20 21 0 21 22 0 22 23 0 23 16 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0
		 31 24 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 32 0 40 41 0 41 42 0
		 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 40 0 48 49 0 49 50 0 50 51 0 51 52 0 52 53 0
		 53 54 0 54 55 0 55 48 0 0 8 0 1 9 0 2 10 0 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 8 16 0
		 9 17 0 10 18 0 11 19 0 12 20 0 13 21 0 14 22 0 15 23 0 16 24 0 17 25 0 18 26 0 19 27 0
		 20 28 0 21 29 0 22 30 0 23 31 0 24 32 0 25 33 0 26 34 0 27 35 0 28 36 0 29 37 0 30 38 0
		 31 39 0 32 40 0 33 41 0 34 42 0 35 43 0 36 44 0 37 45 0 38 46 0 39 47 0 40 48 0 41 49 0
		 42 50 0 43 51 0 44 52 0 45 53 0 46 54 0 47 55 0 48 56 0 49 56 0 50 56 0 51 56 0 52 56 0
		 53 56 0 54 56 0 55 56 0 57 7 0 58 6 0 59 5 0 60 4 0 61 3 0 62 2 0 63 1 0 64 0 0 57 58 0
		 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 63 64 0 64 57 0 63 65 0 64 66 0 65 66 1 62 67 0
		 67 65 1 61 68 1 68 67 1 60 69 0 69 68 1 59 70 0 70 69 1 58 71 0 71 70 1 57 72 1 72 71 1
		 66 72 1 65 82 0 66 83 0 73 74 0 75 74 0 75 73 0 67 89 0 76 73 0 75 76 0 68 88 0 77 76 0
		 75 77 0 69 87 0 78 77 0 75 78 0 70 86 0 79 78 0 75 79 0 71 85 0 80 79 0 75 80 0 72 84 0
		 81 80 0;
	setAttr ".ed[166:215]" 75 81 0 74 81 0 82 90 0 83 97 0 84 96 0 85 95 0 86 94 0
		 87 93 0 88 92 0 89 91 0 82 83 1 83 84 1 84 85 1 85 86 1 86 87 1 87 88 1 88 89 1 89 82 1
		 90 98 0 91 99 0 92 100 0 93 101 0 94 102 0 95 103 0 96 104 0 97 105 0 90 91 1 91 92 1
		 92 93 1 93 94 1 94 95 1 95 96 1 96 97 1 97 90 1 98 73 0 99 76 0 100 77 0 101 78 0
		 102 79 0 103 80 0 104 81 0 105 74 0 98 99 1 99 100 1 100 101 1 101 102 1 102 103 1
		 103 104 1 104 105 1 105 98 1;
	setAttr -s 112 -ch 432 ".fc[0:111]" -type "polyFaces" 
		f 4 0 57 -9 -57
		mu 0 4 55 56 57 58
		f 4 1 58 -10 -58
		mu 0 4 56 59 60 57
		f 4 2 59 -11 -59
		mu 0 4 59 61 136 60
		f 4 3 60 -12 -60
		mu 0 4 121 63 64 62
		f 4 4 61 -13 -61
		mu 0 4 63 65 66 64
		f 4 5 62 -14 -62
		mu 0 4 65 67 68 66
		f 4 6 63 -15 -63
		mu 0 4 67 69 135 68
		f 4 7 56 -16 -64
		mu 0 4 123 55 58 70
		f 4 8 65 -17 -65
		mu 0 4 58 57 71 72
		f 4 9 66 -18 -66
		mu 0 4 57 60 73 71
		f 4 10 67 -19 -67
		mu 0 4 60 136 134 73
		f 4 11 68 -20 -68
		mu 0 4 62 64 75 74
		f 4 12 69 -21 -69
		mu 0 4 64 66 76 75
		f 4 13 70 -22 -70
		mu 0 4 66 68 77 76
		f 4 14 71 -23 -71
		mu 0 4 68 135 133 77
		f 4 15 64 -24 -72
		mu 0 4 70 58 72 78
		f 4 16 73 -25 -73
		mu 0 4 72 71 79 80
		f 4 17 74 -26 -74
		mu 0 4 71 73 81 79
		f 4 18 75 -27 -75
		mu 0 4 73 134 132 81
		f 4 19 76 -28 -76
		mu 0 4 74 75 83 82
		f 4 20 77 -29 -77
		mu 0 4 75 76 84 83
		f 4 21 78 -30 -78
		mu 0 4 76 77 85 84
		f 4 22 79 -31 -79
		mu 0 4 77 133 131 85
		f 4 23 72 -32 -80
		mu 0 4 78 72 80 86
		f 4 24 81 -33 -81
		mu 0 4 80 79 87 88
		f 4 25 82 -34 -82
		mu 0 4 79 81 89 87
		f 4 26 83 -35 -83
		mu 0 4 81 132 130 89
		f 4 27 84 -36 -84
		mu 0 4 82 83 91 90
		f 4 28 85 -37 -85
		mu 0 4 83 84 92 91
		f 4 29 86 -38 -86
		mu 0 4 84 85 93 92
		f 4 30 87 -39 -87
		mu 0 4 85 131 129 93
		f 4 31 80 -40 -88
		mu 0 4 86 80 88 94
		f 4 32 89 -41 -89
		mu 0 4 88 87 95 96
		f 4 33 90 -42 -90
		mu 0 4 87 89 97 95
		f 4 34 91 -43 -91
		mu 0 4 89 130 128 97
		f 4 35 92 -44 -92
		mu 0 4 90 91 99 98
		f 4 36 93 -45 -93
		mu 0 4 91 92 100 99
		f 4 37 94 -46 -94
		mu 0 4 92 93 101 100
		f 4 38 95 -47 -95
		mu 0 4 93 129 127 101
		f 4 39 88 -48 -96
		mu 0 4 94 88 96 102
		f 4 40 97 -49 -97
		mu 0 4 96 95 103 104
		f 4 41 98 -50 -98
		mu 0 4 95 97 105 103
		f 4 42 99 -51 -99
		mu 0 4 97 128 126 105
		f 4 43 100 -52 -100
		mu 0 4 98 99 107 106
		f 4 44 101 -53 -101
		mu 0 4 99 100 108 107
		f 4 45 102 -54 -102
		mu 0 4 100 101 109 108
		f 4 46 103 -55 -103
		mu 0 4 101 127 124 109
		f 4 47 96 -56 -104
		mu 0 4 102 96 104 110
		f 3 146 -148 148
		mu 0 3 45 1 2
		f 3 150 -149 151
		mu 0 3 3 0 2
		f 3 153 -152 154
		mu 0 3 4 3 2
		f 3 156 -155 157
		mu 0 3 5 4 2
		f 3 159 -158 160
		mu 0 3 6 5 2
		f 3 162 -161 163
		mu 0 3 7 6 2
		f 3 165 -164 166
		mu 0 3 8 7 2
		f 3 167 -167 147
		mu 0 3 1 8 2
		f 3 48 105 -105
		mu 0 3 104 103 125
		f 3 49 106 -106
		mu 0 3 103 105 125
		f 3 50 107 -107
		mu 0 3 105 126 125
		f 3 51 108 -108
		mu 0 3 106 107 111
		f 3 52 109 -109
		mu 0 3 107 108 111
		f 3 53 110 -110
		mu 0 3 108 109 111
		f 3 54 111 -111
		mu 0 3 109 124 111
		f 3 55 104 -112
		mu 0 3 110 104 125
		f 4 -7 -114 -121 112
		mu 0 4 69 67 112 113
		f 4 -6 -115 -122 113
		mu 0 4 67 65 114 112
		f 4 -5 -116 -123 114
		mu 0 4 65 63 115 114
		f 4 -4 -117 -124 115
		mu 0 4 63 121 120 115
		f 4 -3 -118 -125 116
		mu 0 4 61 59 117 116
		f 4 -2 -119 -126 117
		mu 0 4 59 56 118 117
		f 4 -1 -120 -127 118
		mu 0 4 56 55 119 118
		f 4 -8 -113 -128 119
		mu 0 4 55 123 122 119
		f 4 126 129 -131 -129
		mu 0 4 54 53 50 52
		f 4 125 128 -133 -132
		mu 0 4 11 9 10 12
		f 4 124 131 -135 -134
		mu 0 4 13 11 12 14
		f 4 123 133 -137 -136
		mu 0 4 15 13 14 16
		f 4 122 135 -139 -138
		mu 0 4 17 15 16 18
		f 4 121 137 -141 -140
		mu 0 4 19 17 18 20
		f 4 120 139 -143 -142
		mu 0 4 21 19 20 22
		f 4 127 141 -144 -130
		mu 0 4 53 21 22 50
		f 4 176 169 199 -169
		mu 0 4 49 24 51 48
		f 4 183 168 192 -176
		mu 0 4 26 23 25 27
		f 4 182 175 193 -175
		mu 0 4 28 26 27 29
		f 4 181 174 194 -174
		mu 0 4 30 28 29 31
		f 4 180 173 195 -173
		mu 0 4 32 30 31 33
		f 4 179 172 196 -172
		mu 0 4 34 32 33 35
		f 4 178 171 197 -171
		mu 0 4 36 34 35 37
		f 4 177 170 198 -170
		mu 0 4 24 36 37 51
		f 4 130 145 -177 -145
		mu 0 4 52 50 24 49
		f 4 143 164 -178 -146
		mu 0 4 50 22 36 24
		f 4 142 161 -179 -165
		mu 0 4 22 20 34 36
		f 4 140 158 -180 -162
		mu 0 4 20 18 32 34
		f 4 138 155 -181 -159
		mu 0 4 18 16 30 32
		f 4 136 152 -182 -156
		mu 0 4 16 14 28 30
		f 4 134 149 -183 -153
		mu 0 4 14 12 26 28
		f 4 132 144 -184 -150
		mu 0 4 12 10 23 26
		f 4 -193 184 208 -186
		mu 0 4 27 25 38 39
		f 4 -194 185 209 -187
		mu 0 4 29 27 39 40
		f 4 -195 186 210 -188
		mu 0 4 31 29 40 41
		f 4 -196 187 211 -189
		mu 0 4 33 31 41 42
		f 4 -197 188 212 -190
		mu 0 4 35 33 42 43
		f 4 -198 189 213 -191
		mu 0 4 37 35 43 44
		f 4 -199 190 214 -192
		mu 0 4 51 37 44 47
		f 4 -200 191 215 -185
		mu 0 4 48 51 47 46
		f 4 -209 200 -151 -202
		mu 0 4 39 38 0 3
		f 4 -210 201 -154 -203
		mu 0 4 40 39 3 4
		f 4 -211 202 -157 -204
		mu 0 4 41 40 4 5
		f 4 -212 203 -160 -205
		mu 0 4 42 41 5 6
		f 4 -213 204 -163 -206
		mu 0 4 43 42 6 7
		f 4 -214 205 -166 -207
		mu 0 4 44 43 7 8
		f 4 -215 206 -168 -208
		mu 0 4 47 44 8 1
		f 4 -216 207 -147 -201
		mu 0 4 46 47 1 45;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube1";
	rename -uid "B7798EAE-4707-F92D-0437-0984723004EE";
	setAttr ".t" -type "double3" 126.26206286166376 0 0 ;
	setAttr ".s" -type "double3" 593.01591157483074 1 123.51364016210744 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "926A948C-4827-3991-467A-EE9BAD8CF8A1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49606239795684814 0.27493694424629211 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 48 ".uvst[0].uvsp[0:47]" -type "float2" 0.85427988 0.70854688
		 0.13784492 0.80577725 0.85427988 0.80577725 0.13784492 0.89422297 0.85427988 0.89422297
		 0.4960624 0.89422297 0.4960624 0.80577725 0.24531013 0.89422297 0.24531013 0.80577725
		 0.37068635 0.89422297 0.37068635 0.80577725 0.63934958 0.89422297 0.63934958 0.80577725
		 0.74681461 0.89422297 0.74681461 0.80577725 -0.14892071 0.15723687 0.13039264 0.27493694
		 -0.14892071 0.27493694 0.85427988 0.15723687 1.1453495 0.15723687 1.1453495 0.27493694
		 0.86184323 0.27493694 0.13784492 0.26294306 0.85427988 0.28620419 0.4960624 0.26316813
		 0.63934958 0.15723687 0.4960624 0.70854688 0.64009643 0.28631961 0.24531013 0.70854688
		 0.37037474 0.28668863 0.24531013 0.26316813 0.37068635 0.15723687 0.13784492 0.28620532
		 0.24494959 0.28546259 0.13784492 0.70854688 0.13784492 0.15723687 0.24531013 0.15723687
		 0.37068635 0.2631681 0.4960624 0.15723687 0.37068635 0.70854688 0.49653608 0.28543195
		 0.63934958 0.26316813 0.74681461 0.15723687 0.63934958 0.70854688 0.74713987 0.28544715
		 0.74681461 0.26316813 0.85427988 0.26316813 0.74681467 0.70854688;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 35 ".vt[0:34]"  -0.72296369 -116.48666382 0.5 0.54728043 -116.48666382 0.5
		 -0.72296369 0.5 -3.29743505 0.54728043 0.5 -3.29743505 -0.72296369 -116.48666382 -3.29743505
		 0.54728043 -116.48666382 -3.29743505 -0.08784163 -116.48666382 0.70494938 -0.08784163 -116.48666382 -3.29743505
		 -0.08784163 0.5 -3.29743505 -0.53242707 -116.48666382 0.76795578 -0.53242707 -116.48666382 -3.29743505
		 -0.53242707 0.5 -3.29743505 -0.31013435 -116.48666382 0.38253504 -0.31013435 0.5 -3.29743505
		 -0.31013435 -116.48666382 -3.29743505 0.16620718 -116.48666382 0.44386077 0.16620718 0.5 -3.29743528
		 0.16620718 -116.48666382 -3.29743528 0.35674387 -116.48666382 0.76761913 0.35674387 0.5 -3.29743528
		 0.35674387 -116.48666382 -3.29743528 -0.72296369 0.5 0.40131485 -0.72296369 -11.19749641 0.5
		 -0.53306627 0.5 0.66837168 -0.53242707 -11.19749641 0.76795578 -0.08784163 -11.19749641 0.70494938
		 -0.087001607 0.5 0.6072346 0.16620718 -11.19749641 0.44386077 0.16753133 0.5 0.3456485
		 -0.31013435 -11.19749641 0.38253504 -0.3106868 0.5 0.28280059 0.35674387 -11.19749641 0.76761913
		 0.35731953 0.5 0.66813517 0.54728043 -11.19749641 0.5 0.54728043 0.5 0.4013246;
	setAttr -s 65 ".ed[0:64]"  0 9 0 2 11 0 4 10 0 0 22 0 1 33 0 2 4 0 3 5 0
		 4 0 0 5 1 0 6 15 0 7 17 0 8 16 0 6 7 1 7 8 1 9 12 0 10 14 0 11 13 0 9 10 1 10 11 1
		 12 6 0 13 8 0 14 7 0 13 14 1 14 12 1 15 18 0 16 19 0 17 20 0 16 17 1 17 15 1 18 1 0
		 19 3 0 20 5 0 19 20 1 20 18 1 21 2 0 22 21 0 34 3 0 34 33 0 22 24 0 24 23 1 23 21 0
		 24 29 0 29 30 1 30 23 0 25 26 1 26 30 0 29 25 0 25 27 0 27 28 1 28 26 0 27 31 0 31 32 1
		 32 28 0 31 33 0 34 32 0 25 6 1 15 27 0 8 26 1 28 16 1 11 23 1 30 13 1 24 9 0 12 29 0
		 18 31 0 32 19 1;
	setAttr -s 32 -ch 130 ".fc[0:31]" -type "polyFaces" 
		f 4 13 11 27 -11
		mu 0 4 6 26 43 12
		f 4 12 10 28 -10
		mu 0 4 5 6 12 11
		f 5 -9 -7 -37 37 -5
		mu 0 5 18 19 20 21 46
		f 5 7 3 35 34 5
		mu 0 5 15 35 22 16 17
		f 4 17 15 23 -15
		mu 0 4 7 8 10 9
		f 4 18 16 22 -16
		mu 0 4 8 28 39 10
		f 4 2 -18 -1 -8
		mu 0 4 1 8 7 3
		f 4 1 -19 -3 -6
		mu 0 4 34 28 8 1
		f 4 -23 20 -14 -22
		mu 0 4 10 39 26 6
		f 4 -24 21 -13 -20
		mu 0 4 9 10 6 5
		f 4 -28 25 32 -27
		mu 0 4 12 43 47 14
		f 4 -29 26 33 -25
		mu 0 4 11 12 14 13
		f 4 -33 30 6 -32
		mu 0 4 14 47 0 2
		f 4 -34 31 8 -30
		mu 0 4 13 14 2 4
		f 4 -36 38 39 40
		mu 0 4 32 22 30 33
		f 4 -40 41 42 43
		mu 0 4 33 30 37 29
		f 4 44 45 -43 46
		mu 0 4 24 40 29 37
		f 4 -45 47 48 49
		mu 0 4 40 24 41 27
		f 4 -49 50 51 52
		mu 0 4 27 41 45 44
		f 4 -52 53 -38 54
		mu 0 4 44 45 46 23
		f 4 55 9 56 -48
		mu 0 4 24 38 25 41
		f 4 57 -50 58 -12
		mu 0 4 26 40 27 43
		f 4 59 -44 60 -17
		mu 0 4 28 33 29 39
		f 4 61 14 62 -42
		mu 0 4 30 36 31 37
		f 4 -41 -60 -2 -35
		mu 0 4 32 33 28 34
		f 4 0 -62 -39 -4
		mu 0 4 35 36 30 22
		f 4 -63 19 -56 -47
		mu 0 4 37 31 38 24
		f 4 -61 -46 -58 -21
		mu 0 4 39 29 40 26
		f 4 -57 24 63 -51
		mu 0 4 41 25 42 45
		f 4 -59 -53 64 -26
		mu 0 4 43 27 44 47
		f 4 -64 29 4 -54
		mu 0 4 45 42 18 46
		f 4 -65 -55 36 -31
		mu 0 4 47 44 23 0;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "C0C0003C-4087-F179-56B5-7881043965A6";
	setAttr -s 6 ".lnk";
	setAttr -s 6 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "F29D8C2B-4851-37FF-3E3A-1EB9C56CEEE3";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "6BE3D4E7-43AC-C1BC-2EF3-82A832B7A3E9";
createNode displayLayerManager -n "layerManager";
	rename -uid "D15CC0B2-4C79-B782-F0F0-1F937E39B527";
	setAttr ".cdl" 3;
	setAttr -s 4 ".dli[1:3]"  1 3 2;
	setAttr -s 4 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "3F88994D-481A-DA0F-3C1C-FA9D7829EB85";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "82416451-462A-9A69-086F-1EAC7BF58368";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "EB57116E-42C0-2172-B6D6-838087619A29";
	setAttr ".g" yes;
createNode lambert -n "reference_mat";
	rename -uid "37DCD117-4CC0-051F-7743-049D52A66BA0";
createNode shadingEngine -n "lambert2SG";
	rename -uid "37C21A90-491C-A94B-891B-D2A1C9B3CE8F";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "CA90E924-4CEE-C837-295B-4A903C239E44";
createNode file -n "reference_texture";
	rename -uid "8B537D42-4412-6557-3B32-F5A7CEEE6262";
	setAttr ".ftn" -type "string" "D:/`School/AI/AI-A2/Fisher-front.png";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture1";
	rename -uid "E6FA1A10-46DD-B7CF-FAE4-06AC14AFA908";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "47CA0921-4DD5-2C96-8288-7AB7CA53A3B2";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 789\n            -height 396\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 789\n            -height 396\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 1\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 789\n            -height 396\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 789\n            -height 396\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n"
		+ "            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n"
		+ "            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n"
		+ "            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n"
		+ "                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n"
		+ "                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n"
		+ "                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n"
		+ "                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -highlightConnections 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n"
		+ "                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 789\\n    -height 396\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 789\\n    -height 396\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 789\\n    -height 396\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 789\\n    -height 396\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 789\\n    -height 396\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 789\\n    -height 396\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 1\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 789\\n    -height 396\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 1\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 789\\n    -height 396\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "FC6863A4-4B60-2D66-64BF-18B36E181732";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode displayLayer -n "reference";
	rename -uid "4CA156E0-45FA-1FD9-DBB1-9F8EE367BAF7";
	setAttr ".dt" 2;
	setAttr ".v" no;
	setAttr ".do" 1;
createNode displayLayer -n "geometry";
	rename -uid "33D4581D-4325-2273-F87A-A781A8EB11BF";
	setAttr ".do" 2;
createNode lambert -n "fisher1_lambert";
	rename -uid "A19D86FD-4801-DD91-F6B0-3B9BE78E8184";
createNode shadingEngine -n "lambert3SG";
	rename -uid "56FB05EB-4CF8-C181-A448-589D0534A7EF";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "9353A7E4-4340-0201-CA82-E9B5AE8BEDAA";
createNode file -n "fisher1_texture";
	rename -uid "0411A1B4-4853-DDA7-35DE-1BACF394D8C5";
	setAttr ".ftn" -type "string" "G:/`FILES/`School/AI/AI-A2/Models/Fisher1_texture.tga";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture2";
	rename -uid "84030BEB-4063-98A5-1419-7BA4988359CE";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "A89984E0-427F-61F0-DB37-FFBF9921AAAA";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -330.95236780151544 -323.80951094248991 ;
	setAttr ".tgi[0].vh" -type "double2" 317.85713022663526 338.09522466054096 ;
createNode lambert -n "fisher2_lambert";
	rename -uid "F88153AC-4788-E0CC-726A-949D15A9A997";
createNode shadingEngine -n "lambert4SG";
	rename -uid "45E664D7-4D23-F0CD-4CE9-74B007EA71F8";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
	rename -uid "F8FB1E50-4DFA-BB0E-AF08-F5ACD28460D7";
createNode file -n "file1";
	rename -uid "668CE258-4FDE-3DCC-4A37-E1BBBD8CFE9D";
	setAttr ".ftn" -type "string" "G:/`FILES/`School/AI/AI-A2/Models/Fisher2_texture.tga";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture3";
	rename -uid "A8F113AE-4D21-AFBE-4C9B-08A4C046437B";
createNode displayLayer -n "ground";
	rename -uid "EAABA888-4659-FEB3-EF5B-209FABF6E25E";
	setAttr ".v" no;
	setAttr ".do" 3;
createNode lambert -n "ground_lambert";
	rename -uid "26CCCEEA-496B-C9F5-0E41-98B5CE093498";
createNode shadingEngine -n "lambert5SG";
	rename -uid "C22626FA-450A-EC63-41FE-199B6B830D5F";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo4";
	rename -uid "14C8D21B-4776-770E-DF68-609CBF8583A1";
createNode file -n "file2";
	rename -uid "2EC10B9F-4E19-A9FF-4BC7-4AB6F36272D5";
	setAttr ".ftn" -type "string" "G:/`FILES/`School/AI/AI-A2/Models/Ground_texture.tga";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture4";
	rename -uid "6009D4F5-4221-075D-BC33-24AC48319ABB";
createNode polyBevel3 -n "polyBevel1";
	rename -uid "F36E85D3-49E2-0A44-36B3-9BBBD147EFA2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[56:119]" "e[128:129]" "e[131]" "e[133]" "e[135]" "e[137]" "e[139]" "e[141]" "e[144:145]" "e[147:149]" "e[151:152]" "e[154:155]" "e[157:158]" "e[160:161]" "e[163:164]" "e[166]" "e[168:175]" "e[184:191]" "e[200:207]";
	setAttr ".ix" -type "matrix" 22.709308686544048 0 0 0 0 21.514081930637055 0 0 0 0 21.514081930637055 0
		 0 77.328395513976375 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 1;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyBevel3 -n "polyBevel2";
	rename -uid "B0F62F1E-46BC-9152-BE89-6A9A09B91B2A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 16 "e[5]" "e[15]" "e[25]" "e[35]" "e[45]" "e[55]" "e[65]" "e[75]" "e[345]" "e[347]" "e[349]" "e[351]" "e[353]" "e[355]" "e[357]" "e[359]";
	setAttr ".ix" -type "matrix" 22.709308686544048 0 0 0 0 21.514081930637055 0 0 0 0 21.514081930637055 0
		 0 77.328395513976375 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.19999999999999996;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyBevel3 -n "polyBevel3";
	rename -uid "8D1A481D-48D4-63C5-0F8A-68842B6B8015";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[0]" "e[5]" "e[10]" "e[15]" "e[20]" "e[25]" "e[30]" "e[35]" "e[304:311]";
	setAttr ".ix" -type "matrix" 22.709308686544048 0 0 0 0 21.514081930637055 0 0 0 0 21.514081930637055 0
		 0 77.328395513976375 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.19999999999999996;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "6754983C-45D6-129A-7DFE-579F268F8EE9";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[226]" -type "float2" -0.01110715 -0.0087688267 ;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 6 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 8 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 4 ".u";
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
	setAttr -s 4 ".tx";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "reference.di" "reference_front.do";
connectAttr "geometry.di" "head.do";
connectAttr "polyTweakUV1.out" "headShape.i";
connectAttr "polyTweakUV1.uvtk[0]" "headShape.uvst[0].uvtw";
connectAttr "ground.di" "pCube1.do";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "reference_texture.oc" "reference_mat.c";
connectAttr "reference_mat.oc" "lambert2SG.ss";
connectAttr "reference_frontShape.iog" "lambert2SG.dsm" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "reference_mat.msg" "materialInfo1.m";
connectAttr "reference_texture.msg" "materialInfo1.t" -na;
connectAttr ":defaultColorMgtGlobals.cme" "reference_texture.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "reference_texture.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "reference_texture.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "reference_texture.ws";
connectAttr "place2dTexture1.c" "reference_texture.c";
connectAttr "place2dTexture1.tf" "reference_texture.tf";
connectAttr "place2dTexture1.rf" "reference_texture.rf";
connectAttr "place2dTexture1.mu" "reference_texture.mu";
connectAttr "place2dTexture1.mv" "reference_texture.mv";
connectAttr "place2dTexture1.s" "reference_texture.s";
connectAttr "place2dTexture1.wu" "reference_texture.wu";
connectAttr "place2dTexture1.wv" "reference_texture.wv";
connectAttr "place2dTexture1.re" "reference_texture.re";
connectAttr "place2dTexture1.of" "reference_texture.of";
connectAttr "place2dTexture1.r" "reference_texture.ro";
connectAttr "place2dTexture1.n" "reference_texture.n";
connectAttr "place2dTexture1.vt1" "reference_texture.vt1";
connectAttr "place2dTexture1.vt2" "reference_texture.vt2";
connectAttr "place2dTexture1.vt3" "reference_texture.vt3";
connectAttr "place2dTexture1.vc1" "reference_texture.vc1";
connectAttr "place2dTexture1.o" "reference_texture.uv";
connectAttr "place2dTexture1.ofs" "reference_texture.fs";
connectAttr "layerManager.dli[1]" "reference.id";
connectAttr "layerManager.dli[3]" "geometry.id";
connectAttr "fisher1_texture.oc" "fisher1_lambert.c";
connectAttr "fisher1_lambert.oc" "lambert3SG.ss";
connectAttr "headShape.iog" "lambert3SG.dsm" -na;
connectAttr "lambert3SG.msg" "materialInfo2.sg";
connectAttr "fisher1_lambert.msg" "materialInfo2.m";
connectAttr "fisher1_texture.msg" "materialInfo2.t" -na;
connectAttr ":defaultColorMgtGlobals.cme" "fisher1_texture.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "fisher1_texture.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "fisher1_texture.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "fisher1_texture.ws";
connectAttr "place2dTexture2.c" "fisher1_texture.c";
connectAttr "place2dTexture2.tf" "fisher1_texture.tf";
connectAttr "place2dTexture2.rf" "fisher1_texture.rf";
connectAttr "place2dTexture2.mu" "fisher1_texture.mu";
connectAttr "place2dTexture2.mv" "fisher1_texture.mv";
connectAttr "place2dTexture2.s" "fisher1_texture.s";
connectAttr "place2dTexture2.wu" "fisher1_texture.wu";
connectAttr "place2dTexture2.wv" "fisher1_texture.wv";
connectAttr "place2dTexture2.re" "fisher1_texture.re";
connectAttr "place2dTexture2.of" "fisher1_texture.of";
connectAttr "place2dTexture2.r" "fisher1_texture.ro";
connectAttr "place2dTexture2.n" "fisher1_texture.n";
connectAttr "place2dTexture2.vt1" "fisher1_texture.vt1";
connectAttr "place2dTexture2.vt2" "fisher1_texture.vt2";
connectAttr "place2dTexture2.vt3" "fisher1_texture.vt3";
connectAttr "place2dTexture2.vc1" "fisher1_texture.vc1";
connectAttr "place2dTexture2.o" "fisher1_texture.uv";
connectAttr "place2dTexture2.ofs" "fisher1_texture.fs";
connectAttr "file1.oc" "fisher2_lambert.c";
connectAttr "fisher2_lambert.oc" "lambert4SG.ss";
connectAttr "lambert4SG.msg" "materialInfo3.sg";
connectAttr "fisher2_lambert.msg" "materialInfo3.m";
connectAttr "file1.msg" "materialInfo3.t" -na;
connectAttr ":defaultColorMgtGlobals.cme" "file1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file1.ws";
connectAttr "place2dTexture3.c" "file1.c";
connectAttr "place2dTexture3.tf" "file1.tf";
connectAttr "place2dTexture3.rf" "file1.rf";
connectAttr "place2dTexture3.mu" "file1.mu";
connectAttr "place2dTexture3.mv" "file1.mv";
connectAttr "place2dTexture3.s" "file1.s";
connectAttr "place2dTexture3.wu" "file1.wu";
connectAttr "place2dTexture3.wv" "file1.wv";
connectAttr "place2dTexture3.re" "file1.re";
connectAttr "place2dTexture3.of" "file1.of";
connectAttr "place2dTexture3.r" "file1.ro";
connectAttr "place2dTexture3.n" "file1.n";
connectAttr "place2dTexture3.vt1" "file1.vt1";
connectAttr "place2dTexture3.vt2" "file1.vt2";
connectAttr "place2dTexture3.vt3" "file1.vt3";
connectAttr "place2dTexture3.vc1" "file1.vc1";
connectAttr "place2dTexture3.o" "file1.uv";
connectAttr "place2dTexture3.ofs" "file1.fs";
connectAttr "layerManager.dli[2]" "ground.id";
connectAttr "file2.oc" "ground_lambert.c";
connectAttr "ground_lambert.oc" "lambert5SG.ss";
connectAttr "pCubeShape1.iog" "lambert5SG.dsm" -na;
connectAttr "lambert5SG.msg" "materialInfo4.sg";
connectAttr "ground_lambert.msg" "materialInfo4.m";
connectAttr "file2.msg" "materialInfo4.t" -na;
connectAttr ":defaultColorMgtGlobals.cme" "file2.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file2.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file2.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file2.ws";
connectAttr "place2dTexture4.c" "file2.c";
connectAttr "place2dTexture4.tf" "file2.tf";
connectAttr "place2dTexture4.rf" "file2.rf";
connectAttr "place2dTexture4.mu" "file2.mu";
connectAttr "place2dTexture4.mv" "file2.mv";
connectAttr "place2dTexture4.s" "file2.s";
connectAttr "place2dTexture4.wu" "file2.wu";
connectAttr "place2dTexture4.wv" "file2.wv";
connectAttr "place2dTexture4.re" "file2.re";
connectAttr "place2dTexture4.of" "file2.of";
connectAttr "place2dTexture4.r" "file2.ro";
connectAttr "place2dTexture4.n" "file2.n";
connectAttr "place2dTexture4.vt1" "file2.vt1";
connectAttr "place2dTexture4.vt2" "file2.vt2";
connectAttr "place2dTexture4.vt3" "file2.vt3";
connectAttr "place2dTexture4.vc1" "file2.vc1";
connectAttr "place2dTexture4.o" "file2.uv";
connectAttr "place2dTexture4.ofs" "file2.fs";
connectAttr "polySurfaceShape1.o" "polyBevel1.ip";
connectAttr "headShape.wm" "polyBevel1.mp";
connectAttr "polyBevel1.out" "polyBevel2.ip";
connectAttr "headShape.wm" "polyBevel2.mp";
connectAttr "polyBevel2.out" "polyBevel3.ip";
connectAttr "headShape.wm" "polyBevel3.mp";
connectAttr "polyBevel3.out" "polyTweakUV1.ip";
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "lambert4SG.pa" ":renderPartition.st" -na;
connectAttr "lambert5SG.pa" ":renderPartition.st" -na;
connectAttr "reference_mat.msg" ":defaultShaderList1.s" -na;
connectAttr "fisher1_lambert.msg" ":defaultShaderList1.s" -na;
connectAttr "fisher2_lambert.msg" ":defaultShaderList1.s" -na;
connectAttr "ground_lambert.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture3.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture4.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "reference_texture.msg" ":defaultTextureList1.tx" -na;
connectAttr "fisher1_texture.msg" ":defaultTextureList1.tx" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file2.msg" ":defaultTextureList1.tx" -na;
// End of Fisher.ma
