﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class RivalPickingTargetFish : StateBehaviour
{
    FloatVar fishingRangeVar;
    GameObjectVar fishManagerVar;
    GameObjectVar targetedFishVar;
    GameObjectVar playerVar;

    private void Awake()
    {
        fishingRangeVar = blackboard.GetFloatVar("fishingRange");
        fishManagerVar = blackboard.GetGameObjectVar("fishManager");
        targetedFishVar = blackboard.GetGameObjectVar("targetedFish");
        playerVar = blackboard.GetGameObjectVar("player");
    }
    
    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *RivalPickingTargetFish*");

        PickTargetFish();

        SendEvent("pickedTargetFish");
    }
    
    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *RivalPickingTargetFish*");
    }

    void PickTargetFish()
    {
        List<GameObject> fishInPond = fishManagerVar.Value.GetComponent<Spawner>().fishInPond;

        if (fishInPond.Count > 0)
        {
            // PRIORITY #1: FISH THAT ARE REALLY CLOSE BY
            if (FishWithinCloseRange(transform.position, fishingRangeVar.Value) == true)
            {
                targetedFishVar.Value = GetClosestFish(fishInPond, transform.position).gameObject;
            }
            // PRIORITY #2: FISH THAT ARE NEAREST TO THE PLAYER'S LURE (because you're rude)
            /// look for closest fish to PLAYER'S LURE
            else if (playerVar.Value.GetComponent<ThrowingLure>().isFishing == true)
            {
                ThrowingLure playerScript = playerVar.Value.GetComponent<ThrowingLure>();

                if (playerScript.lure != null)
                {
                    targetedFishVar.Value = GetClosestFish(fishInPond, playerScript.lure.transform.position).gameObject;
                }
            }
            // PRIORITY #3: CLOSEST FISH AFTER PRIORITY #2
            else
            {
                targetedFishVar.Value = GetClosestFish(fishInPond, transform.position).gameObject;
            }
        }
        else
        {
            SendEvent("foundNoFish");
        }
    }

    bool FishWithinCloseRange(Vector3 center, float radius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(center, radius, ~0, QueryTriggerInteraction.Collide);

        foreach (Collider collider in hitColliders)
        {
            // we just want to check if there are any fish nearby
            if (collider.gameObject.tag == "Fish")
            {
                return true;
            }
        }

        return false;
    }

    GameObject GetClosestFish(List<GameObject> fishList, Vector3 origin)
    {
        GameObject nearestFish = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = origin;

        foreach (GameObject fish in fishList)
        {
            Vector3 directionToTarget = fish.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;

            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                nearestFish = fish;
            }
        }

        return nearestFish;
    }
}


