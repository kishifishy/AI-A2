﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class FishPoking : StateBehaviour
{
    [SerializeField]
    private int maximumPokesBeforeBiting;
    
    private int numberOfPokes, pokesNeededBeforeBiting;

    [SerializeField]
    private float delay;

    FloatVar shakeMagnitudeVar;

    [SerializeField]
    private bool isPoking;

    GameObjectVar lureGameObjectVar;

    NavMeshAgent navMeshAgent;

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();

        shakeMagnitudeVar = blackboard.GetFloatVar("shakeMagnitude");
        lureGameObjectVar = blackboard.GetGameObjectVar("lure");
    }

    void Start()
    {
        numberOfPokes = 0;

        pokesNeededBeforeBiting = Random.Range(2, maximumPokesBeforeBiting + 1);

        isPoking = false;
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *FishPoking*");

        Poke();
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *FishPoking*");
    }

    // Update is called once per frame
    void Update()
    {
        if (lureGameObjectVar.Value != null)
        {
            // check if i should bite
            if (ItsTimeToBite())
            {
                StopAllCoroutines();
                SendEvent("decideToBite");
            }
            // otherwise, just poke the lure!
            else {
                if (isPoking == false)
                {
                    StartCoroutine(Poke());
                    isPoking = true;
                }
            }
        }
        else
        {
            lureGameObjectVar.Value = null;
            SendEvent("loseLure");
        }
    }

    IEnumerator Poke()
    {
        yield return new WaitForSeconds(delay);

        numberOfPokes++;

        iTween.ShakeRotation(this.gameObject, iTween.Hash(
            "y", shakeMagnitudeVar.Value,
            "time", delay*0.75f));
        
        isPoking = false;
    }

    bool ItsTimeToBite()
    {
        if (numberOfPokes >= pokesNeededBeforeBiting)
            return true;
        else
            return false;
    }
}


