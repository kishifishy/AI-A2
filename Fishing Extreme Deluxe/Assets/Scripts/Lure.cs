﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lure : MonoBehaviour {

    public GameObject owner;
    public bool isTargeted;
    public bool isLuring;
    
	void Start () {

        owner = transform.root.gameObject;

        isTargeted = false;
        isLuring = false;

    }
}
