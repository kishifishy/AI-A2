﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class FishSwimmingToLure : StateBehaviour
{
    [SerializeField]
    private float arrivalDistance, speedMultiplier;

    GameObjectVar lureGameObjectVar;

    NavMeshAgent navMeshAgent;
    NavMeshPath navMeshPath;

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshPath = new NavMeshPath();

        lureGameObjectVar = blackboard.GetGameObjectVar("lure");
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *FishSwimmingToLure*");

        // mark lure as Targeted so that other fish can't target the same lure

        lureGameObjectVar.Value.GetComponent<Lure>().isTargeted = true;

        // get speed and "double" it (actually maybe just 1.5x it)
        navMeshAgent.speed *= speedMultiplier;
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *FishSwimmingToLure*");
    }

    // Update is called once per frame
    void Update()
    {
        if (lureGameObjectVar.Value != null)
        {
            // use arrival distance instead of onTriggerEnter so that we can extend the trigger size
            // the vision code provided uses a raycast to the actual game object, so when it hits a trigger ...
            // ... but doesn't hit the game object itself, the code goes haywire
            if (navMeshAgent.remainingDistance <= arrivalDistance)
            {
                navMeshAgent.speed = 0.0f;
                SendEvent("arriveAtLure");
            }

            navMeshAgent.CalculatePath(lureGameObjectVar.Value.transform.position, navMeshPath);
            navMeshAgent.SetPath(navMeshPath);
        }
        else
        {
            lureGameObjectVar.Value = null;
            SendEvent("loseLure");
        }
    }

    void OnVisionEnter()
    {

    }

    void OnVisionExit(GameObject other)
    {
        if (lureGameObjectVar.Value != null)
        {
            lureGameObjectVar.Value.GetComponent<Lure>().isTargeted = false;
            lureGameObjectVar.Value = null;
            SendEvent("loseLure");
        }
    }
}


