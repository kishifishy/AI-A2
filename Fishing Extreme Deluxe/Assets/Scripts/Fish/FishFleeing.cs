﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class FishFleeing : StateBehaviour
{
    [SerializeField]
    private float arrivalDistance;

    [SerializeField]
    private GameObject fleePoint;

    NavMeshAgent navMeshAgent;
    NavMeshPath navMeshPath;

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshPath = new NavMeshPath();
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *FishFleeing*");

        navMeshAgent.speed = 5.0f;

        // we'll put this on OnEnable so it immediately overrides its path towards the lure
        navMeshAgent.CalculatePath(fleePoint.transform.position, navMeshPath);
        navMeshAgent.SetPath(navMeshPath);
    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *FishFleeing*");
    }

    // Update is called once per frame
    void Update()
    {
        if (navMeshAgent.remainingDistance <= arrivalDistance)
        {
            // remove self from list before destroying
            GetComponentInParent<Spawner>().fishInPond.Remove(this.gameObject);

            Destroy(this.gameObject);
        }
        else
        {
            navMeshAgent.CalculatePath(fleePoint.transform.position, navMeshPath);
            navMeshAgent.SetPath(navMeshPath);
        }
    }
}


