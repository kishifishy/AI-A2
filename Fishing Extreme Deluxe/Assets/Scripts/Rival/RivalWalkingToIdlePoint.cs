﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class RivalWalkingToIdlePoint : StateBehaviour
{
    [SerializeField]
    private float arrivalDistance;

    [SerializeField]
    GameObject destination;

    NavMeshAgent navMeshAgent;
    NavMeshPath navMeshPath;

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshPath = new NavMeshPath();
    }

    // Called when the state is enabled
    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *RivalWalkingToIdlePoint*");

        navMeshAgent.speed = 2.5f;

    }

    // Called when the state is disabled
    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *RivalWalkingToIdlePoint*");

        navMeshAgent.speed = 1.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (navMeshAgent.remainingDistance <= arrivalDistance)
        {
            SendEvent("arrivedAtIdlePoint");
        }

        navMeshAgent.CalculatePath(destination.transform.position, navMeshPath);
        navMeshAgent.SetPath(navMeshPath);
    }
}


