﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputMovement : MonoBehaviour
{
    [SerializeField]
    private bool usePhysics;

    [SerializeField]
    private float speed;

    Rigidbody rigidbodyComponent;

    void Awake()
    {
        rigidbodyComponent = GetComponent<Rigidbody>();
    }

    void Start()
    {

    }


    void Update()
    {
        if (usePhysics)
            MoveByAddForce();
        else
            MoveByTranslation();
    }

    void MoveByAddForce()
    {
        Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        rigidbodyComponent.AddForce(input * speed * Time.deltaTime);
    }

    void MoveByTranslation()
    {
        Vector3 input = new Vector3(-Input.GetAxis("Horizontal"), 0, -Input.GetAxis("Vertical")) * speed;

        transform.Translate(input * Time.deltaTime);
    }
}
