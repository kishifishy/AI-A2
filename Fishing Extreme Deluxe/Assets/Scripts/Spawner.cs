﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    [SerializeField]
    private GameObject prefab, parent;

    [SerializeField]
    private Vector3 origin, distanceFromOrigin;

    [SerializeField]
    private float baseSpawnDelay, delayVariation;

    [SerializeField]
    private bool spawnAtStart;

    private float startTime, spawnDelay;
    private int numberOfSpawnedObjects;

    [SerializeField]
    public List<GameObject> fishInPond = new List<GameObject>();

    // Use this for initialization
    void Start () {
        numberOfSpawnedObjects = 0;

        if (spawnAtStart)
            SpawnObject();

        StartTimer();
        RandomizeSpawnDelay();
	}
	
	// Update is called once per frame
	void Update () {
        float elapsedTime = Time.time - startTime;

        if (elapsedTime >= spawnDelay)
        {
            SpawnObject();
            StartTimer();
            RandomizeSpawnDelay();
        }
	}

    void StartTimer ()
    {
        startTime = Time.time;
    }

    void RandomizeSpawnDelay ()
    {
        spawnDelay = baseSpawnDelay + Random.Range(-delayVariation, delayVariation);
    }

    void SpawnObject ()
    {
        numberOfSpawnedObjects++;

        Vector3 spawnPosition = new Vector3(
            origin.x + Random.Range(-distanceFromOrigin.x, distanceFromOrigin.x),
            prefab.transform.position.y,
            origin.z + Random.Range(-distanceFromOrigin.z, distanceFromOrigin.z));

        GameObject gameObject = Instantiate(prefab, spawnPosition, Quaternion.identity);

        // this makes testing easier
        gameObject.name = prefab.name + " " + numberOfSpawnedObjects;

        gameObject.transform.parent = parent.transform;

        // this class would be nice as a tool i can use next time,
        // but this part is specific to this project
        // not sure if i should separate it but this is the best i can do
        fishInPond.Add(gameObject);
    }
}
