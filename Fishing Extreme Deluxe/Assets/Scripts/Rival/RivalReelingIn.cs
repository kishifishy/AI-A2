﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class RivalReelingIn : StateBehaviour
{
    public bool isFishing;
    private bool coroutineRunning = false;

    GameObjectVar lureVar;

    void Awake()
    {
        lureVar = blackboard.GetGameObjectVar("lure");
    }

    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *RivalReelingIn*");

        StartCoroutine(LureAnimation(transform.position, false));
    }

    // Called when the state is disabled
    void OnDisable()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }


    IEnumerator LureAnimation(Vector3 destination, bool spawningLure)
    {
        coroutineRunning = true;

        isFishing = spawningLure;

        // move the lure towards the destination (water or back to player)
        iTween.MoveTo(lureVar.Value, iTween.Hash(
            "position", destination,
            "time", 1.0f));

        yield return new WaitForSeconds(1.0f);

        // if we were reeling in, destroy the lure
        if (!spawningLure)
            Destroy(lureVar.Value);

        coroutineRunning = false;

        SendEvent("caughtFish");
    }
}


