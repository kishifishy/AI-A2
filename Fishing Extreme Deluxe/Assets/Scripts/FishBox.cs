﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishBox : MonoBehaviour
{
    public int fishInsideBox = 0;

    [SerializeField]
    private float shakeMagnitude, shakeDuration;

    [SerializeField]
    TextMesh fishNumberText;

    // Use this for initialization
    void Start()
    {
        fishNumberText = GetComponentInChildren<TextMesh>();
    }

    // Update is called once per frame
    void Update()
    {
        fishNumberText.text = fishInsideBox.ToString();
    }

    public void AddFish(int number)
    {
        fishInsideBox += number;
        StartCoroutine(Shake(number));
    }
    
    IEnumerator Shake(int number)
    {
        iTween.PunchScale(this.gameObject, Vector3.one * shakeMagnitude * Mathf.Sign(number), shakeDuration);

        yield return new WaitForSeconds(shakeDuration);
    }
}
