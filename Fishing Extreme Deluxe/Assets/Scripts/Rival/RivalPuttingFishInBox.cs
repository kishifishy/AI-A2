﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class RivalPuttingFishInBox : StateBehaviour
{
    GameObjectVar rivalFishBoxVar;

    void Awake()
    {
        rivalFishBoxVar = blackboard.GetGameObjectVar("rivalFishBox");
    }
    
    void OnEnable () {
        //Debug.Log("Started *RivalPuttingFishInBox*");

        // kind of like a timer
        StartCoroutine(PutFishAndWalkBack());
	}
 
	void OnDisable () {
        //Debug.Log("Stopped *RivalPuttingFishInBox*");

        StopAllCoroutines();
	}
	
	void Update () {
    }

    IEnumerator PutFishAndWalkBack ()
    {
        yield return new WaitForSeconds(1);

        if (transform.GetChild(0).GetChild(0).childCount > 0)
        {
            rivalFishBoxVar.Value.GetComponent<FishBox>().AddFish(1);
            Destroy(transform.GetChild(0).GetChild(0).GetChild(0).gameObject);
            SendEvent("finishedPuttingFishInBox");
        }

    }
}


