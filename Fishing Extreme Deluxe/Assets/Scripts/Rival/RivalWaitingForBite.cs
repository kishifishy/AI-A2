﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class RivalWaitingForBite : StateBehaviour
{
    [SerializeField]
    private float waitingTime;

    public bool isFishing;
    private bool coroutineRunning = false;

    GameObjectVar lureVar;
    GameObjectVar fishManagerVar;
    GameObjectVar targetedFishVar;

    void Awake()
    {
        lureVar = blackboard.GetGameObjectVar("lure");
        fishManagerVar = blackboard.GetGameObjectVar("fishManager");
        targetedFishVar = blackboard.GetGameObjectVar("targetedFish");
    }

    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *RivalWaitingForBite*");

        // kind of like a countdown to when time's up
        StartCoroutine(GiveUp());
    }

    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *RivalWaitingForBite*");

        isFishing = false;
        coroutineRunning = false;
        StopAllCoroutines();
    }

    void Update()
    {
        if (targetedFishVar.Value != null)
        {
            if (FishIsBiting())
                SendEvent("timeToReelIn");
        }

        // if at any time, the targeted fish disappears, go back to idle
        if (targetedFishVar.Value == null)
        {
            if (lureVar.Value != null)
            {
                StartCoroutine(LureAnimation(transform.position, false));
                if (!coroutineRunning)
                    SendEvent("foundNoFish");
            }
            else
                SendEvent("foundNoFish");
        }
    }

    bool FishIsBiting()
    {
        if (targetedFishVar.Value.GetComponent<StateMachine>().GetEnabledStateName() == "Biting")
            return true;

        return false;
    }

    IEnumerator GiveUp()
    {
        yield return new WaitForSeconds(waitingTime);

        // if fish doesn't bite while you're waiting, give up
        StartCoroutine(LureAnimation(transform.position, false));
        if (coroutineRunning)
            SendEvent("foundNoFish");
    }

    IEnumerator LureAnimation(Vector3 destination, bool spawningLure)
    {
        coroutineRunning = true;

        isFishing = spawningLure;

        // move the lure towards the destination (water or back to player)
        iTween.MoveTo(lureVar.Value, iTween.Hash(
            "position", destination,
            "time", 1.0f));

        yield return new WaitForSeconds(1.0f);

        // if we were reeling in, destroy the lure
        if (!spawningLure)
            Destroy(lureVar.Value);

        coroutineRunning = false;
    }


}


