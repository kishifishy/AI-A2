﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;

public class RivalIdle : StateBehaviour
{
    [SerializeField]
    private int maxChance = 10, waitingTime = 3;

    [SerializeField]
    Transform lookTarget;

    GameObjectVar playerFishBoxVar;
    GameObjectVar rivalFishBoxVar;

    private void Awake()
    {
        playerFishBoxVar = blackboard.GetGameObjectVar("playerFishBox");
        rivalFishBoxVar = blackboard.GetGameObjectVar("rivalFishBox");
    }

    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *RivalIdle*");

        StartCoroutine(DecideNextAction());
    }

    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *RivalIdle*");

        StopAllCoroutines();
    }

    void Update()
    {
        
    }

    IEnumerator DecideNextAction()
    {
        int result = Random.Range(0, maxChance);

        yield return new WaitForSeconds(waitingTime);

        // the bigger the difference between player and rival score is, the higher chance of rival stealing from player
        if (result <= ScoreDifference())
            SendEvent("decideToSteal");
        else
            SendEvent("decideToFish");

    }

    int ScoreDifference()
    {
        int playerScore = playerFishBoxVar.Value.GetComponent<FishBox>().fishInsideBox;
        int rivalScore = rivalFishBoxVar.Value.GetComponent<FishBox>().fishInsideBox;

        int difference = playerScore - rivalScore;

        if (difference > 0)
            return difference;
        else
            return 0;
    }

}


