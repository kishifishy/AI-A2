﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;

public class RivalWalkingToBox : StateBehaviour
{
    [SerializeField]
    GameObject destination;

    GameObjectVar rivalFishBoxVar;

    NavMeshAgent navMeshAgent;
    NavMeshPath navMeshPath;

    void Awake()
    {
        rivalFishBoxVar = blackboard.GetGameObjectVar("rivalFishBox");

        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshPath = new NavMeshPath();
    }
    
    void OnEnable()
    {
        //Debug.Log(this.gameObject.name + " started *RivalWalkingToBox*");

        navMeshAgent.speed = 1.5f;
    }
    
    void OnDisable()
    {
        //Debug.Log(this.gameObject.name + " ended *RivalWalkingToBox*");
    }
    
    void Update()
    {
        navMeshAgent.CalculatePath(destination.transform.position, navMeshPath);
        navMeshAgent.SetPath(navMeshPath);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Fish Box")
        {
            if (other.gameObject == rivalFishBoxVar.Value)
            {
                navMeshAgent.speed = 0;
                SendEvent("arrivedAtBox");
            }
        }
    }
}


